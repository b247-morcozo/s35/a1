const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 4004;

// Allows us to read json data
app.use(express.json());

// Allows us to read data from forms
app.use(express.urlencoded({extended:true}));

	// [ SECTION ] MongoDB Connection
	// Connect to the database by passing in your connection string, remember to replace the password

	// Syntax:
		// mongoose.connect("MongoDB connection string", { useNewUrlParser : true });

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://Kevreg18:143Regina@zuitt-bootcamp.tnlla0e.mongodb.net/s35-discussion?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.on("open", () => console.log("We're connected to the cloud database"));

// [ SECTION ] Moongoose Schemas
	// Schemas determine the structure of the documents to be written in the database

const taskSchema = new mongoose.Schema({
	name : String,
	status : {
		type : String,
		default : "pending"
	}
})

// [ SECTION ] Models
	// Uses schemas and are used to create/instantiate objects that correspond to the schema

// MongoDB Model should always be Capital & Singular
const Task = mongoose.model("Task", taskSchema);

// [ SECTION ] Routes

//Business Logic
/*
	1. Add a functionality to check if there are duplicate tasks
		-If the task already exists in the database, we return an error
		-If the task doesn't exist in the database, we add in the database
	2. The task data will be coming from the request's body
	3. Create a new task object with a "name" field/property
	4. The status property does not need to be provided because our schema default	
*/

app.post("/tasks", (request, response) => {

	// Checks if there are duplicate tasks
	Task.findOne({name: request.body.name}, (error, result) => {
		if(result != null && result.name == request.body.name){
			return response.send('Duplicate task found!')
		} 

		let newTask = new Task({
			name: request.body.name
		})

		newTask.save((error, savedTask) => {
			if(error){
				return console.error(error)
			} else {
				return response.status(200).send('New task created!')
			}
		})
	})
})

// Business Logic
/*
	1. Retrieve all the documents
	2. if an error is encountered, print error
	3. if no errors are found, send a success status back to client/postman and return an array of documents
*/

app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
	if (err) {
		return console.log(err);
	} else {
		return res.status(200).json({
			data : result
		})
	}})
})



// Listen to the port
app.listen(port, () => console.log(`Server is now running at port ${port}`));


//Activity

const userSchema = new mongoose.Schema({
	name : String,
	password : String,
})

const User = mongoose.model("User", userSchema);

app.post("/users", (request, response) => {

	// Checks if there are duplicate tasks
	User.findOne({name: request.body.name}, (error, result) => {
		if(result != null && result.name == request.body.name){
			return response.send('Duplicate user found!')
		} 

		let newUser = new User({
			name: request.body.name,
			password: request.body.password
		})

		newUser.save((error, savedUser) => {
			if(error){
				return console.error(error)
			} else {
				return response.status(200).send('New user added!')
			}
		})
	})
})


app.get("/users", (req, res) => {
	User.find({}, (err, result) => {
	if (err) {
		return console.log(err);
	} else {
		return res.status(200).json({
			data : result
		})
	}})
})
